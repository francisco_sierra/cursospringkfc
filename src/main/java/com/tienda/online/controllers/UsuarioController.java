package com.tienda.online.controllers;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tienda.online.exception.ModelNotFoundException;
import com.tienda.online.models.Usuario;
import com.tienda.online.services.UsuarioServices;

@RestController
@RequestMapping("/api/usuarios")
public class UsuarioController {
	@Autowired
	private UsuarioServices usuarioServices;
	
	@Resource(name="tokenServices")
	private ConsumerTokenServices tokenServices;
	
	@PostMapping
	public ResponseEntity<Usuario> guardar(@RequestBody @Validated Usuario usuario){
		Usuario newUsuario=usuarioServices.guardar(usuario);
		if(newUsuario==null) {
			throw new DataIntegrityViolationException("Ya existe un usuario con email: "+ usuario.getEmail());
		} 
		return ResponseEntity.status(HttpStatus.CREATED).body(newUsuario);
	}
	
	@GetMapping
	public ResponseEntity<List<Usuario>> listar(){
		List<Usuario> lista =usuarioServices.listar();
		return ResponseEntity.status(HttpStatus.OK).body(lista);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Usuario> obtenerPorId(@PathVariable(value = "id") Long id){
		Usuario usuario = usuarioServices.obtenerPorId(id);
		if(usuario==null) {
			throw new ModelNotFoundException("No se encontró el usuario con ID: "+id);
		}
		return new ResponseEntity<Usuario>(usuario,HttpStatus.OK);
	}
	
	@PutMapping
	public ResponseEntity<Usuario> actualizar(@RequestBody @Validated Usuario usuario){
		Usuario updateUsuario = usuarioServices.actualizar(usuario); 
		return new ResponseEntity<Usuario>(updateUsuario,HttpStatus.OK);
	}
	
	@DeleteMapping(path="/{id}")
	public void eliminar(@PathVariable(value="id") Long id){
		usuarioServices.eliminar(id);
	}
}
