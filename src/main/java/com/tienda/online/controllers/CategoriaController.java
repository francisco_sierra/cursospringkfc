package com.tienda.online.controllers;

import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tienda.online.exception.ModelNotFoundException;
import com.tienda.online.models.Categoria;
import com.tienda.online.services.CategoriaServices;

@RestController
@RequestMapping("/api/categorias")
public class CategoriaController {

	private CategoriaServices categoriaServices;

	public CategoriaController(CategoriaServices categoriaServices) {
		this.categoriaServices = categoriaServices;
	}
	
	@PostMapping
	public ResponseEntity<Categoria> guardar(@RequestBody @Validated Categoria categoria){
		Categoria newCategoria=categoriaServices.guardar(categoria);
		if(newCategoria==null) {
			throw new DataIntegrityViolationException("Ya existe un categoria con nombre: "+ categoria.getNombre());
		} 
		return ResponseEntity.status(HttpStatus.CREATED).body(newCategoria);
	}
	
	@GetMapping
	public ResponseEntity<List<Categoria>> listar(){
		List<Categoria> lista =categoriaServices.listar();
		return ResponseEntity.status(HttpStatus.OK).body(lista);
	}
	
	@PutMapping
	public ResponseEntity<Categoria> actualizar(@RequestBody @Validated Categoria categoria){
		Categoria updateCategoria = categoriaServices.actualizar(categoria); 
		return new ResponseEntity<Categoria>(updateCategoria,HttpStatus.OK);
	}
	
	@DeleteMapping(path="/{id}")
	public void eliminar(@PathVariable(value="id") String id){
		categoriaServices.eliminar(id);
	}
}
