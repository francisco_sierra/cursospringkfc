package com.tienda.online.controllers;

import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tienda.online.exception.ModelNotFoundException;
import com.tienda.online.models.Articulo;
import com.tienda.online.services.ArticuloServices;

//@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/api/articulos")
public class ArticuloController {

	private ArticuloServices articuloServices;

	public ArticuloController(ArticuloServices articuloServices) {
		this.articuloServices = articuloServices;
	}
	
	@PostMapping
	public ResponseEntity<Articulo> guardar(@RequestBody @Validated Articulo articulo){
		Articulo newArticulo=articuloServices.guardar(articulo);
		if(newArticulo==null) {
			throw new DataIntegrityViolationException("Ya existe un articulo con nombre: "+ articulo.getNombre());
		} 
		return ResponseEntity.status(HttpStatus.CREATED).body(newArticulo);
	}
	
	@GetMapping
	public ResponseEntity<List<Articulo>> listar(){
		List<Articulo> lista =articuloServices.listar();
		return ResponseEntity.status(HttpStatus.OK).body(lista);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Articulo> obtenerPorId(@PathVariable(value = "id") Long id){
		Articulo articulo = articuloServices.obtenerPorId(id);
		if(articulo==null) {
			throw new ModelNotFoundException("No se encontró el articulo con ID: "+id);
		}
		return new ResponseEntity<Articulo>(articulo,HttpStatus.OK);
	}
	
	@PutMapping
	public ResponseEntity<Articulo> actualizar(@RequestBody @Validated Articulo articulo){
		Articulo updateArticulo = articuloServices.actualizar(articulo); 
		return new ResponseEntity<Articulo>(updateArticulo,HttpStatus.OK);
	}
	
	@DeleteMapping(path="/{id}")
	public void eliminar(@PathVariable(value="id") Long id){
		articuloServices.eliminar(id);
	}
}
