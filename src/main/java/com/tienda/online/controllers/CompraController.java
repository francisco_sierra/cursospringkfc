package com.tienda.online.controllers;

import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tienda.online.exception.ModelNotFoundException;
import com.tienda.online.models.Compra;
import com.tienda.online.services.CompraServices;

@RestController
@RequestMapping("/api/compras")
public class CompraController {

	private CompraServices compraServices;

	public CompraController(CompraServices compraServices) {
		this.compraServices = compraServices;
	}
	
	@PostMapping
	public ResponseEntity<Compra> guardar(@RequestBody Compra compra){
		Compra newCompra=compraServices.guardar(compra);
		return ResponseEntity.status(HttpStatus.CREATED).body(newCompra);
	}
	
	@GetMapping
	public ResponseEntity<List<Compra>> listar(){
		List<Compra> lista =compraServices.listar();
		return ResponseEntity.status(HttpStatus.OK).body(lista);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Compra> obtenerPorId(@PathVariable(value = "id") Long id){
		Compra compra = compraServices.obtenerPorId(id);
		if(compra==null) {
			throw new ModelNotFoundException("No se encontró una compra con ID: "+id);
		}
		return new ResponseEntity<Compra>(compra,HttpStatus.OK);
	}
	
	@PutMapping
	public ResponseEntity<Compra> actualizar(@RequestBody @Validated Compra compra){
		Compra updateCompra = compraServices.actualizar(compra); 
		return new ResponseEntity<Compra>(updateCompra,HttpStatus.OK);
	}
	
	@DeleteMapping(path="/{id}")
	public void eliminar(@PathVariable(value="id") Long id){
		compraServices.eliminar(id);
	}
}
