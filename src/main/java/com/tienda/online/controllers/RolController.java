package com.tienda.online.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tienda.online.models.Rol;
import com.tienda.online.services.RolServices;

@RestController
@RequestMapping("/api/roles")
public class RolController {

	private RolServices rolServices;

	public RolController(RolServices rolServices) {
		this.rolServices = rolServices;
	}
	
	@PostMapping
	public Rol guardar(@RequestBody Rol rol){
		return rolServices.guardar(rol);
	}
	
	@GetMapping
	public List<Rol> listar(){
		return rolServices.listar();
	}
	
	@PutMapping
	public Rol actualizar(@RequestBody Rol rol){
		return rolServices.actualizar(rol);
	}
	
	@DeleteMapping(path="/{id}")
	public void eliminar(@PathVariable(value="id") Long id){
		rolServices.eliminar(id);
	}
}
