package com.tienda.online.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.tienda.online.models.Articulo;

//Le avisa a Spring que esta clase va a manejar el repositorio de Rol
//en CrudRepository<Rol, Long>,
//el primer parámetro es el nombre de la clase que se va a manejar en el repositorio
//el segundo parametro es el tipo de campo de la clave primaria
@Repository
public interface ArticuloRepository extends CrudRepository<Articulo, Long>{
	
	Articulo findByNombre(String nombre);
	
	//Consultas personalizadas a la entidad
	@Query("Select a from Articulo a where categoria.nombre = :categoriaNombre and a.precio <= :precio and a.cantidad >= :cantidad")
	List<Articulo> reporteCliente(String categoriaNombre, float precio, Integer cantidad );
	
}