package com.tienda.online.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.tienda.online.models.Rol;
import com.tienda.online.models.Usuario;

//Le avisa a Spring que esta clase va a manejar el repositorio de Rol
//en CrudRepository<Rol, Long>,
//el primer parámetro es el nombre de la clase que se va a manejar en el repositorio
//el segundo parametro es el tipo de campo de la clave primaria
@Repository
public interface UsuarioRepository extends CrudRepository<Usuario, Long>{
	Usuario findByEmail(String email);
	Optional<Usuario> findOneByEmailIgnoreCase(String correo);
}