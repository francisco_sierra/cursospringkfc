package com.tienda.online.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.tienda.online.models.Categoria;
import com.tienda.online.repositories.CategoriaRepository;

@Service
public class CategoriaServices {

	//Inyección de dependencias mediante atributo
	private CategoriaRepository categoriaRepository;

	public CategoriaServices(CategoriaRepository categoriaRepository) {
		this.categoriaRepository = categoriaRepository;
	}
	
	public Categoria guardar(Categoria categoria) {
		if(categoriaRepository.findById(categoria.getId()).isPresent()) {
			return null;
		}
		
		return categoriaRepository.save(categoria);
	}
	
	public List<Categoria> listar(){
		return (List<Categoria>) categoriaRepository.findAll();
	}
	
	public Categoria actualizar(Categoria categoria) {
		return categoriaRepository.save(categoria);
	}
	
	public void eliminar(String id) {
		categoriaRepository.deleteById(id);
	}
}
