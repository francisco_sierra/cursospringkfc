package com.tienda.online.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.tienda.online.models.Rol;
import com.tienda.online.repositories.RolRepository;

@Service
public class RolServices {

	//Inyección de dependencias mediante atributo
	private RolRepository rolRepository;

	public RolServices(RolRepository rolRepository) {
		this.rolRepository = rolRepository;
	}
	
	public Rol guardar(Rol rol) {
		if(rolRepository.findByNombre(rol.getNombre())!=null) {
			return null;
		}
		
		return rolRepository.save(rol);
	}
	
	public List<Rol> listar(){
		return (List<Rol>) rolRepository.findAll();
	}
	
	public Rol actualizar(Rol rol) {
		return rolRepository.save(rol);
	}
	
	public void eliminar(Long id) {
		rolRepository.deleteById(id);
	}
}
