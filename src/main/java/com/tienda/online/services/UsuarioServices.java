package com.tienda.online.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.tienda.online.models.Usuario;
import com.tienda.online.repositories.UsuarioRepository;

@Service("userDetailsService")
public class UsuarioServices implements UserDetailsService{

	//Inyección de dependencias mediante atributo
	private UsuarioRepository usuarioRepository;
	private BCryptPasswordEncoder encoder;
	
	public UsuarioServices(UsuarioRepository usuarioRepository,BCryptPasswordEncoder encoder) {
		this.usuarioRepository = usuarioRepository;
		this.encoder = encoder;
	}
	
	public Usuario guardar(Usuario usuario) {
		usuario.setFecha(new Date());
		usuario.setPassword(encoder.encode(usuario.getPassword()));
		if(usuarioRepository.findByEmail(usuario.getEmail())!=null) {
			return null;
		}
		
		return usuarioRepository.save(usuario);
	}
	
	public List<Usuario> listar(){
		return (List<Usuario>) usuarioRepository.findAll();
	}
	
	public Usuario actualizar(Usuario usuario) {
		usuario.setPassword(encoder.encode(usuario.getPassword()));
		return usuarioRepository.save(usuario);
	}
	
	public void eliminar(Long id) {
		usuarioRepository.deleteById(id);
	}
	
	public Usuario obtenerPorId(Long id) {
		
		return usuarioRepository.findById(id).isPresent() ? usuarioRepository.findById(id).get() : null;
	}
	
	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		Usuario usuario = usuarioRepository.findOneByEmailIgnoreCase(email).get();
		if (usuario == null) {
			throw new UsernameNotFoundException(email);
		}

		List<GrantedAuthority> authorities = new ArrayList<>();
		authorities.add(new SimpleGrantedAuthority(usuario.getRol().getNombre()));
	
		UserDetails userDetails = new User(usuario.getEmail(), usuario.getPassword(), authorities);
		return userDetails;
	}
}
