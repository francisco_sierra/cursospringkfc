package com.tienda.online.services;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.tienda.online.models.Articulo;
import com.tienda.online.models.Compra;
import com.tienda.online.models.Usuario;
import com.tienda.online.repositories.ArticuloRepository;
import com.tienda.online.repositories.CompraRepository;

@Service
public class CompraServices {

	//Inyección de dependencias mediante atributo
	private CompraRepository compraRepository;
	private ArticuloRepository articuloRepository;

	public CompraServices(CompraRepository compraRepository) {
		this.compraRepository = compraRepository;
		this.articuloRepository = articuloRepository;
	}
	
	public Compra guardar(Compra compra) {
		compra.setFecha(new Date());
		compra.setDocumento("0000-0000");
		compra.getDetalleCompraList().forEach(detalle ->{
			Optional<Articulo> articulo = articuloRepository.findById(detalle.getArticulo().getId());
			if(articulo.isPresent()) {
				articulo.get().setCantidad(articulo.get().getCantidad() - detalle.getCantidad());
				articuloRepository.save(articulo.get());
				detalle.setCompra(compra);
			}
		});
		
		return compraRepository.save(compra);
	}
	
	public List<Compra> listar(){
		return (List<Compra>) compraRepository.findAll();
	}
	
	public Compra actualizar(Compra compra) {
		return compraRepository.save(compra);
	}
	
	public void eliminar(Long id) {
		compraRepository.deleteById(id);
	}
	
	public Compra obtenerPorId(Long id) {
		return compraRepository.findById(id).isPresent() ? compraRepository.findById(id).get() : null;
	}
}
