package com.tienda.online.services;

import java.util.List;
import org.springframework.stereotype.Service;
import com.tienda.online.models.Articulo;
import com.tienda.online.models.Usuario;
import com.tienda.online.repositories.ArticuloRepository;

@Service
public class ArticuloServices {

	//Inyección de dependencias mediante atributo
	private ArticuloRepository articuloRepository;

	public ArticuloServices(ArticuloRepository articuloRepository) {
		this.articuloRepository = articuloRepository;
	}
	
	public Articulo guardar(Articulo articulo) {
		if(articuloRepository.findByNombre(articulo.getNombre())!=null) {
			return null;
		}
		
		return articuloRepository.save(articulo);
	}
	
	public List<Articulo> listar(){
		return (List<Articulo>) articuloRepository.findAll();
	}
	
	public Articulo actualizar(Articulo articulo) {
		return articuloRepository.save(articulo);
	}
	
	public void eliminar(Long id) {
		articuloRepository.deleteById(id);
	}
	
	public Articulo obtenerPorId(Long id) {
		
		return articuloRepository.findById(id).isPresent() ? articuloRepository.findById(id).get() : null;
	}

}
